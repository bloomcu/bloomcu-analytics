<?php
/*
Plugin Name: BloomCU Analytics
Plugin URI: https://bloomcu.com/
Description: Analytics events editor. A WordPress Plugin by BloomCU.
Version: 1.0.1
Author: BloomCU
Author URI: https://bloomcu.com/
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: analyticsplugin
Domain Path: /languages
*/

/**
 * Copyright (c) YEAR Your Name (email: ). All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

// don't call the file directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Analytics_Plugin class
 *
 * @class Analytics_Plugin The class that holds the entire Analytics_Plugin plugin
 */
final class Analytics_Plugin {

	/**
	 * Plugin version
	 *
	 * @var string
	 */
	public $version = '1.0.1';

	/**
	 * Holds various class instances
	 *
	 * @var array
	 */
	private $container = array();

	/**
	 * Constructor for the Analytics_Plugin class
	 *
	 * Sets up all the appropriate hooks and actions
	 * within our plugin.
	 */
	public function __construct() {

		$this->define_constants();

		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		add_action( 'plugins_loaded', array( $this, 'init_plugin' ) );
	}

	/**
	 * Initializes the Analytics_Plugin() class
	 *
	 * Checks for an existing Analytics_Plugin() instance
	 * and if it doesn't find one, creates it.
	 */
	public static function init() {
		static $instance = false;

		if ( ! $instance ) {
			$instance = new Analytics_Plugin();
		}

		return $instance;
	}

	/**
	 * Magic getter to bypass referencing plugin.
	 *
	 * @param $prop
	 *
	 * @return mixed
	 */
	public function __get( $prop ) {
		if ( array_key_exists( $prop, $this->container ) ) {
			return $this->container[ $prop ];
		}

		return $this->{$prop};
	}

	/**
	 * Magic isset to bypass referencing plugin.
	 *
	 * @param $prop
	 *
	 * @return mixed
	 */
	public function __isset( $prop ) {
		return isset( $this->{$prop} ) || isset( $this->container[ $prop ] );
	}

	/**
	 * Define the constants
	 *
	 * @return void
	 */
	public function define_constants() {
		define( 'ANALYTICSPLUGIN_VERSION', $this->version );
		define( 'ANALYTICSPLUGIN_FILE', __FILE__ );
		define( 'ANALYTICSPLUGIN_PATH', dirname( ANALYTICSPLUGIN_FILE ) );
		define( 'ANALYTICSPLUGIN_INCLUDES', ANALYTICSPLUGIN_PATH . '/includes' );
		define( 'ANALYTICSPLUGIN_URL', plugins_url( '', ANALYTICSPLUGIN_FILE ) );
		define( 'ANALYTICSPLUGIN_ASSETS', ANALYTICSPLUGIN_URL . '/assets' );
	}

	/**
	 * Load the plugin after all plugins are loaded
	 *
	 * @return void
	 */
	public function init_plugin() {
		$this->includes();
		$this->init_hooks();
	}

	/**
	 * Placeholder for activation function
	 *
	 * Nothing being called here yet.
	 */
	public function activate() {

		$installed = get_option( 'analyticsplugin_installed' );

		if ( ! $installed ) {
			update_option( 'analyticsplugin_installed', time() );
		}

		update_option( 'analyticsplugin_version', ANALYTICSPLUGIN_VERSION );

		// Insert default terms
		// wp_insert_term('Loan Payment - General', 'analytics_type');
	}

	/**
	 * Placeholder for deactivation function
	 *
	 * Nothing being called here yet.
	 */
	public function deactivate() {

	}

	/**
	 * Include the required files
	 *
	 * @return void
	 */
	public function includes() {

		require_once ANALYTICSPLUGIN_INCLUDES . '/class-assets.php';

		if ( $this->is_request( 'admin' ) ) {
			require_once ANALYTICSPLUGIN_INCLUDES . '/class-admin.php';
		}

		if ( $this->is_request( 'frontend' ) ) {
			require_once ANALYTICSPLUGIN_INCLUDES . '/class-frontend.php';
		}

		// require_once ANALYTICSPLUGIN_INCLUDES . '/class-ajax.php';

		require_once ANALYTICSPLUGIN_INCLUDES . '/class-rest-api.php';

		require_once ANALYTICSPLUGIN_INCLUDES . '/class-post-type.php';

	}

	/**
	 * Initialize the hooks
	 *
	 * @return void
	 */
	public function init_hooks() {

		add_action( 'init', array( $this, 'init_classes' ) );

		// Localize our plugin
		add_action( 'init', array( $this, 'localization_setup' ) );

		$this->init_cpt();
	}

	/**
	 * Instantiate the required classes
	 *
	 * @return void
	 */
	public function init_classes() {

		if ( $this->is_request( 'admin' ) ) {
			$this->container['admin'] = new Analytics\Admin();
		}

		if ( $this->is_request( 'frontend' ) ) {
			$this->container['frontend'] = new Analytics\Frontend();
		}

		$this->container['assets'] = new Analytics\Assets();

		// $this->container['ajax'] = new Analytics\Ajax();

		$this->container['rest'] = new Analytics\REST_API();
	}

	/**
	 * Initialize our custom post type
	 */
	public function init_cpt() {
		new Analytics\Post_Type();
	}

	/**
	 * Initialize plugin for localization
	 *
	 * @uses load_plugin_textdomain()
	 */
	public function localization_setup() {
		load_plugin_textdomain( 'analyticsplugin', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
	}

	/**
	 * Default posts
	 * Creates default posts when plugin is activated if post's don't exist
	 *
	 * @return void
	 */
	public function create_default_posts() {

		// Initialize the post ID to -1. This indicates no action has been taken.
		$post_id = -1;

		// Setup the author, slug, and title for the post
		$author_id = 1;
		$title = 'My Example Post';
		$slug = 'example-post';

		// If the page doesn't already exist, then create it
		if( null == get_page_by_title( $title ) ) {

			// Set the post ID so that we know the post was created successfully
			$post_id = wp_insert_post(
				array(
					'comment_status' =>	'closed',
					'ping_status'	 =>	'closed',
					'post_author'	 =>	$author_id,
					'post_name'		 =>	$slug,
					'post_title'	 =>	$title,
					'post_status'	 =>	'publish',
					'post_type'		 =>	'analytics'
				)
			);

		// Otherwise, we'll stop
		} else {

	    		// Arbitrarily use -2 to indicate that the page with the title already exists
	    		$post_id = -2;

		} // end if

	} // end create_default_posts

	/**
	 * What type of request is this?
	 *
	 * @param  string $type admin, ajax, cron or frontend.
	 *
	 * @return bool
	 */
	private function is_request( $type ) {
		switch ( $type ) {
			case 'admin':
				return is_admin();

			case 'ajax':
				return defined( 'DOING_AJAX' );

			case 'rest':
				return defined( 'REST_REQUEST' );

			case 'cron':
				return defined( 'DOING_CRON' );

			case 'frontend':
				return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
		}
	}


} // Analytics_Plugin

$analyticsplugin = Analytics_Plugin::init();

/**
 * Init update checker
 * @link https://github.com/YahnisElsts/plugin-update-checker
 */
// require 'plugin-update-checker-4.4/plugin-update-checker.php';

// $update_checker = Puc_v4_Factory::buildUpdateChecker(
// 	'https://analytics.bloomcudev.com/analytics.json',
// 	__FILE__,
// 	'analytics'
// );
