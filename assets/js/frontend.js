pluginWebpack([0],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(1);

__webpack_require__(2);

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

/*
 * FORMAT NUMBER
 * ----
 * This function is used to add thousand seperators to numerical ouput
 * as a means of properly formatting money
 */
function formatNumber(num) {

    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

/*
 * GET INPUT VALUE
 * ----
 * This function checks the type of input being used and gets its value
 */
function getInputValue(inputClass) {

    // Input, Slider or Select
    if ($(inputClass).is('input:text') || $(inputClass).is('input[type=range]') || $(inputClass).is('select')) {
        return $(inputClass).val();

        // Buttons
    } else if ($(inputClass).is('input:radio')) {
        return $(inputClass + ':checked').val();
    }
}

/*
 * PAYMENT ANALYTICS
 * ----
 * This function determines your monthly payment
 * using a payment ammount, rate and term.
 */
function calculate_payment() {

    // Get output
    var result = $('.analytics-payment .output-result');

    /**
    * Get data from inputs
    */

    // Amount
    var amount = getInputValue('.input-amount');
    console.log('amount=', amount);

    // Rate
    var rate = getInputValue('.input-rate');
    console.log('rate=', rate);

    // Term
    var term = getInputValue('.input-term');
    console.log('term=', term);

    // Credit
    // var credit = getInputValue('.input-credit');
    // console.log('credit=', credit);

    // Modify rate if credit score exists
    // if ( credit != undefined ) {
    //     // Modify rate based on credit modifier
    //     // rate = Number(rate) + Number(credit);
    //     rate = credit;
    // }

    // var credit_modifier = $('.analytics-payment .input-credit-modifier').val();
    // // TODO
    // if ( credit_modifier != undefined ) {
    //     // Modify rate based on credit modifier
    //     rate = Number(rate) + Number(credit_modifier);
    // }

    // Calculate interest
    var monthly_interest = rate / 100 / 12;

    // Calculate monthly payment
    var payment = Math.pow(1 + monthly_interest, term),
        monthly = amount * payment * monthly_interest / (payment - 1);

    // Format result
    var payment_formatted = monthly.toFixed(2);

    // Output result
    result.html('$' + formatNumber(payment_formatted) + '/mo');
}

/*
 * AFFORD CALCULATION
 * ----
 * This function determines how much money you can afford to borrow
 * using a payment ammount, rate and term.
 */
function calculate_afford() {

    // Get outputs
    var result = $('.analytics-afford .output-result');

    // Get inputs
    var payment = $('.analytics-afford .input-payment').val(),
        rate = $('.analytics-afford .input-rate').val(),
        term = $('.analytics-afford .input-term').val(),
        credit = $('.analytics-afford .input-credit').val();

    // Modify rate if credit score exists
    if (credit != undefined) {
        rate = Number(rate) + Number(credit);
    }

    // Calculate interest
    var monthly_interest = rate / 100 / 12,
        annual_interest = rate / 100;

    // Calculate amount result
    var amount = payment * (1 - Math.pow(1 + monthly_interest, -1 * term)) * (12 / annual_interest);

    // Format result
    var amount_formatted = amount.toFixed(2);

    // Output result
    result.html('$' + formatNumber(amount_formatted));
}

// Init afford analytics
calculate_afford();

// Listen for afford analytics field changes
$(".analytics-afford .calc-input").on('input', function () {
    calculate_afford();
});

/*
 * INIT
 * ----
 * This function initializes things
 */
function analyticsInit() {

    // Select all input content on focus
    $('.calc-input:text').focus(function () {
        $(this).select();
    });

    /**
    * Payment Calculator
    */
    // Init
    calculate_payment();

    // Listen for radio buttons selection
    $(".analytics-payment .calc-input:radio").change(function () {
        calculate_payment();
    });

    // Listen for all other inputs
    $(".analytics-payment .calc-input").on('input', function () {
        calculate_payment();
    });
}

$(document).ready(function () {
    analyticsInit();
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ })
],[0]);