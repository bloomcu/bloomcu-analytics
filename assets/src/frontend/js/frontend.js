//--------------------------------------------------------------
// ANALYTICS FRONTEND
//--------------------------------------------------------------

var pausing_bloom_inspect = false;

/**
*
* Global Analytics Utilities
* ------------------------------------
* ------------------------------------
*
*/

/*
* Get URL Parameter
*
*/
function GetUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

/*
* Cookie Handelers
*
*/
function setCookie( name, value, days) {
    if ( days ) {
		var date = new Date();
		date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
		var expires = "; expires="+date.toGMTString();
	}
	else {
        var expires = "";
    }
    document.cookie = name + "=" + value + ";expires=" + expires + ";path=/";
}

function getCookie( name ) {
	var nameEQ = name + "=";
	var ca = document.cookie.split( ';' );
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while ( c.charAt(0)==' ' ) c = c.substring( 1, c.length );
		if ( c.indexOf( nameEQ ) == 0 ) return c.substring( nameEQ.length,c.length );
	}
	return null;
}

function eraseCookie( name ) {
	setCookie( name, "", -1);
}

/*
* Highlight Targetable Elements
*
*/
function HighlightOn() {

    // Unpause inspect mode
    pausing_bloom_inspect = false;

    // Switch on
    SwitchOn();

    var i,
        elements = document.querySelectorAll('a, button, form'),
        total = elements.length;

    for ( i = 0; i < total; i++ ) {

        // var tag = elements[i].tagName.toLowerCase()

        // Check for class
        var isBloom = elements[i].classList.contains('is-bloom-app-element');

        if ( !isBloom ) {
            elements[i].style.borderWidth = '2px';
            elements[i].style.borderStyle = 'dashed';
            elements[i].style.borderColor = '#E8006E';

            elements[i].classList.add('bloom-target');
        }

    }

};

function HighlightOff() {

    // Pause inspect mode
    pausing_bloom_inspect = true;

    // Switch off
    SwitchOff();

    var i,
        elements = document.querySelectorAll('a, button, form'),
        total = elements.length;

    for ( i = 0; i < total; i++ ) {

        // Check for class
        var isBloom = elements[i].classList.contains('is-bloom-app-element');

        if ( !isBloom ) {
            elements[i].style.borderWidth = 'initial';
            elements[i].style.borderStyle = 'initial';
            elements[i].style.borderColor = 'initial';
        }

    }

};

/*
* Switch Highlighting On/Off
*
*/
function SwitchOn() {

    // Switch on
    $('.bloom-switch-container-switch input').prop( "checked", true );

    // Switch text
    $('.bloom-switch-container-switch-label').text('On');

}

function SwitchOff() {

    // Switch on
    $('.bloom-switch-container-switch input').prop( "checked", false );

    // Switch text
    $('.bloom-switch-container-switch-label').text('Pausing');

}

/*
* Targetable Elements Hover
*
*/
function InspectMouseOver(event) {

    var element = event.target;

    // Not WP Admin Bar items
    if ( !element.classList.contains('ab-item') ) {

        var tag = element.tagName.toLowerCase();

        if ( tag === 'a' || tag === 'button') {

          element.style.borderStyle = 'solid';

        }

    }

};

function InspectMouseOut(event) {

    var element = event.target;

    // Not WP Admin Bar items
    if ( !element.classList.contains('ab-item') ) {

        var tag = element.tagName.toLowerCase();

        if ( tag === 'a' || tag === 'button' ) {

          element.style.borderStyle = 'dashed';

        }

    }

};

/*
* Inspect Targetable Element
*
*/
function InspectClick( event ) {

    if ( !pausing_bloom_inspect ) {
        event.preventDefault();
        var clicked_element = $(event.target);

        // These are elements used in this bloom analytics interface
        var bloom_app_element = clicked_element[0].classList.contains('is-bloom-app-element');
        if ( !bloom_app_element ) {

            event.stopPropagation();

            // Get target element
            var blooom_target = clicked_element.closest('.bloom-target');
            if ( blooom_target.length ) {

                // Toggle views
                $('#bloom-sidebar-main').hide();
                $('#bloom-sidebar-new').fadeIn();

                /**
                * Trigger
                *
                */
                // Get selected trigger from goalmode
                var trigger = $('.bloom-card bloom-single-trigger, .selected').data("trigger");

                /**
                * Label
                *
                */
                // Get target label
                var label = blooom_target[0].innerText;

                /**
                * Element Parent
                *
                */
                // Get nearest parent classname
                var el_parent = '.' + blooom_target[0].closest('div').className.replace(/\s+/g, '.');

                /**
                * Element Tag
                *
                */
                // Get selected element's tagname
                var el_tag = blooom_target[0].tagName.toLowerCase();

                /**
                * Element Identifier
                *
                */

                // Get selected element's class
                var el_identifier = blooom_target[0].className;

                console.log('classname check', el_identifier);

                /*
                 * If no classname(s)
                 *
                 */
                if (el_identifier == 'bloom-target') {

                    console.log('no classname', el_identifier);

                    // Get selected element's id
                    var el_identifier = blooom_target[0].id;

                    /*
                     * If id
                     *
                     */
                    if (el_identifier) {

                        // Replace any spaces with '#'
                        el_identifier = el_identifier.replace(/\s+/g, '#');

                        // Prepend with '#'
                        el_identifier = '#' + el_identifier;

                        console.log('has id', el_identifier);

                    /*
                     * If inner text
                     *
                     */
                    } else if ( blooom_target[0].text !== undefined ) {

                        // Use inner text as identifier
                        el_identifier = ':contains(' + blooom_target[0].text + ')';

                    /*
                     * If nothing
                     *
                     */
                    } else {

                        el_identifier = '';

                    }

                /*
                 * Has classname(s)
                 *
                 */
                } else {

                    // Replace spaces with '.'
                    el_identifier = el_identifier.replace(/\s+/g, '.');

                    // Prepend with '.'
                    el_identifier = '.' + el_identifier;

                    // Remove '.bloom-target' and 'bloom-target' instances from class list
                    el_identifier = el_identifier.replace('.bloom-target', '').replace('bloom-target', '');

                }

                console.log(el_identifier);


                /**
                * Selector
                *
                */
                // Build selector
                var selector = el_parent + ' ' + el_tag + el_identifier;

                console.log('----------------------');
                console.log('el_parent:', el_parent);
                console.log('el_tag:', el_tag);
                console.log('el_identifier:', el_identifier);
                console.log('selector:', selector);

                // Fill goal form
                $('#trigger').text(trigger);
                $('#element').text(el_tag);
                $('#selector').text(selector);
                $('#label').text(label);

            }

        }

    }

    return false;
};

/**
*
* Goal Create/Delete API
* ------------------------------------
* ------------------------------------
*
*/

/**
* ----------------
* Create Goal
* ----------------
*/
function CreateGoal( $el ) {

    // Change save button state
    $el.text('Creating...');

    /**
    * Get the data
    */
    var trigger = $('#trigger').text(),
        element = $('#element').text(),
        selector = $('#selector').text(),
        label = $('#label').text(),
        pageId = $('#page-id').text(),
        pageName = $('#page-name').text();

    /**
    * Make request
    */
    // $.getJSON(analyticsLocal.siteUrl + '/wp-json/analytics/v1/create-goal?_wpnonce=' + analyticsLocal.nonce + '&' + params, (result) => {
    $.getJSON(analyticsLocal.siteUrl + '/wp-json/analytics/v1/create-goal?_wpnonce=' + analyticsLocal.nonce, {
        trigger: trigger,
        element: element,
        selector: selector,
        label: label,
        pageId: pageId,
        pageName: pageName,
    }, (result) => {

        // API function complete
        console.log('Created new goal with id: ', result);

        // Create dom element for new goal
        var new_goal = `
            <div class="bloom-single-goal bloom-card">
                <div class="bloom-card-left">
                    <svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="16.7000017" height="16.7000017"></rect>
                            <path d="M3.07754319,7.35412918 C3.39062659,7.56729234 3.47056277,8.00028002 3.25739961,8.32002477 C3.0375751,8.63310817 2.60458742,8.71304435 2.29150402,8.49321984 C1.07913852,7.66721258 0.353051491,6.28831336 0.353051491,4.81615525 C0.353051491,3.67706459 0.792700518,2.53131258 1.66533722,1.66533722 C2.53131258,0.792700518 3.67706459,0.353051491 4.81615525,0.353051491 C5.96190726,0.353051491 7.10099792,0.792700518 7.97363463,1.66533722 C8.16681374,1.85185499 8.34000881,2.06501816 8.49321984,2.29150402 C8.71304435,2.60458742 8.63310817,3.0375751 8.32002477,3.25739961 C8.00028002,3.47056277 7.56729234,3.39062659 7.35412918,3.07754319 C7.2475476,2.92433216 7.12098197,2.77778249 6.98775499,2.64455551 C6.38823359,2.04503411 5.60219442,1.74527341 4.81615525,1.74527341 C4.03011608,1.74527341 3.24407691,2.04503411 2.64455551,2.64455551 C2.04503411,3.24407691 1.74527341,4.03011608 1.74527341,4.81615525 C1.74527341,5.82201893 2.25153593,6.78125317 3.07754319,7.35412918 Z M4.88943009,4.00347069 L15.8739944,7.78045551 C16.2337073,7.90702114 16.4268864,8.30004072 16.3069821,8.66641491 C16.2337073,8.86625538 16.0871576,9.01280505 15.9006398,9.08607989 L11.0511778,11.0511778 L9.08607989,15.9073012 C8.93953022,16.267014 8.53318793,16.4335477 8.18013644,16.2936594 C7.98695732,16.2137232 7.847069,16.0605122 7.78045551,15.8739944 L4.00347069,4.88943009 C3.88356641,4.52971725 4.07674552,4.13003631 4.43645836,4.00347069 C4.58966939,3.95684124 4.74954176,3.95684124 4.88943009,4.00347069 L4.88943009,4.00347069 Z" id="Shape" fill="#E8006E" fill-rule="nonzero"></path>
                        </g>
                    </svg>
                </div>
                <div class="bloom-card-body">
                    <div class="bloom-card-body-label">${label.toLowerCase()}</div>
                    <div class="bloom-card-body-trigger">${trigger}</div>
                </div>
                <div class="bloom-card-right">
                    <button class="bloom-single-goal-delete is-bloom-app-element" data-id="${result}">
                        <svg width="12px" height="12px" viewBox="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
                                <path d="M1.08910393,1.2962281 L10.4978082,10.7049324" stroke="#000000" stroke-width="1.5"></path>
                                <path d="M1.08910393,1.2962281 L10.4978082,10.7049324" stroke="#000000" stroke-width="1.5" transform="translate(5.811612, 6.018737) scale(-1, 1) translate(-5.811612, -6.018737) "></path>
                            </g>
                        </svg>
                    </button>
                </div>
            </div>
        `;

        // Hide empy list message
        $('.bloom-goal-list-message').hide();

        // Add new goal to list
        $('.bloom-goal-list').append(new_goal)

        // Toggle views
        $('#bloom-sidebar-new').hide();
        $('#bloom-sidebar-main').fadeIn();

        // Set listener on new element
        SetNewListeners()

    })
    .done(function() {

        // Succeeded
        console.log('Create request done');
    })
    .fail(function(jqXHR, textStatus, errorThrown) {

        // Failed
        console.log('Create request failed... ' + textStatus);

        // Reset save button
        $el.text('Save');
    })
    .always(function() {

        // Finished
        console.log('Create request ended');

        // Reset save button
        $el.text('Save');

    });

}

/**
* ----------------
* Delete Goal
* ----------------
*/
function DeleteGoal( $el ) {

    // Change delete button state
    $el.text('...');

    // Get goal id from data attribute
    var goalId = $el.data("id");

    console.log('Deleting:', goalId);

    /**
    * Build params url
    */
    var params = 'goalId=' + goalId;

    /**
    * Make request
    */
    $.getJSON(analyticsLocal.siteUrl + '/wp-json/analytics/v1/delete-goal?_wpnonce=' + analyticsLocal.nonce + '&' + params, (result) => {

        // API function complete
        console.log('API returned: ', result);
    })
    .done(function() {

        // Succeeded
        console.log('Delete request succeeded!');
    })
    .fail(function(jqXHR, textStatus, errorThrown) {

        // Failed
        console.log('Delete request failed! ' + textStatus);
        $el.text('x');
    })
    .always(function() {

        // Finished
        console.log('Delete request ended!');
        $el.closest('.bloom-single-goal').fadeOut();
    });

}

/**
* ----------------
* Refresh Listeners
* ----------------
*/
function SetNewListeners() {

    // Single goal delete buttons
    $(".bloom-single-goal-delete").on("click", function() {
        DeleteGoal( $(this) );
    });
}

/**
*
* Init/Stop
* ------------------------------------
* ------------------------------------
*
*/

/*
* Init goal mode
*
*/
function InitGoalMode() {

    // Get this page url
    var page = window.location.href;

    // Set goal mode cookie
    setCookie('bloom_goalmode_on', true, 7);

    // Redirect to this page in goal mode
    window.location = page;
}

/*
* Stop goal mode
*
*/
function StopGoalMode() {

    // Get this page url
    var page = window.location.href;

    // Erase goal mode cookie
    eraseCookie('bloom_goalmode_on');

    // Redirect to thispage in goal mode
    window.location = page;
}

/*
* Is goal mode on?
*
*/
var goal_mode_on_cookie = getCookie('bloom_goalmode_on');

if ( goal_mode_on_cookie ) {

    // Highlight goalable elements
    HighlightOn();

    // Listen to all clicks
    document.addEventListener("click", InspectClick, true);

    // Listen to Create Goal button
    $(".bloom-sidebar-footer-create").on("click", function() {
        CreateGoal( $(this) );
    });

    // Listen to Delete Goal buttons
    $(".bloom-single-goal-delete").on("click", function() {
        DeleteGoal( $(this) );
    });

    // Listen to trigger selectors on new goal view
    $(".bloom-single-trigger").on("click", function() {

        // Style selection
        $('.bloom-single-trigger').removeClass('selected');
        $(this).addClass('selected');

        // Get selected trigger
        var trigger = $(this).data("trigger");

        // Fill goal form
        $('#trigger').text(trigger);

    });

    // Listen to new goal view close button
    $(".bloom-close-sidebar-new").on("click", function() {
        // Toggle views
        $('#bloom-sidebar-new').hide();
        $('#bloom-sidebar-main').fadeIn();
    });

    // Listen to goal mode stop icon
    $(".bloom-goalmode-stop").on("click", function() {
        StopGoalMode();
    });

    // Listen to pause switch click
    $(".bloom-switch-container-switch").on("click", function() {

        var pauseSwitch = $(".bloom-switch-container-switch input");

        if ( pausing_bloom_inspect ) {
            HighlightOn();
        } else {
            HighlightOff();
        }
    });

    // All keydowns
    window.onkeydown = function(event) {

        // If already pausing, stop
        if ( pausing_bloom_inspect ) { return false; }

        if ( event.which === 83 ) {
            HighlightOff();
        }
    }

    // All keyups
    window.onkeyup = function(event) {

        if ( event.which === 83 ) {
            HighlightOn();
        }
    }

} else {
    // Not in goal mode

    // Listen to Goal Mode trigger dot
    $(".bloom-goalmode-trigger-dot").on("click", function() {
        InitGoalMode();
    });

};
