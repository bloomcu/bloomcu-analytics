<?php
namespace Analytics;

/**
 * Custom Post Type
 */
class Post_Type {

	public function __construct() {
		add_action( 'init', [ $this, 'analytics_post_type' ], 5 );
		// add_action( 'init', [ $this, 'analytics_type_taxonomy' ], 5 );
		add_action( 'init', [ $this, 'analytics_acf' ], 5 );
	}

	/**
	 * Register custom post type
	 *
	 * @return void
	 */
	public function analytics_post_type() {

		$labels = [
			'name'          => __( 'Analytics' ),
			'singular_name' => __( 'Analytics' ),
			'add_new'       => __( 'Add New' ),
			'add_new_item'  => __( 'Add New Trigger' ),
			'edit_item'     => __( 'Edit Trigger' ),
			'not_found'     => __( 'None Found, click that add button' ),
		];

		$supports = [
			'title',
		];

		$args = [
			'labels'              => $labels,
			'public'              => true,
			'publicly_queryable'  => false,
			'has_archive'         => false,
			'menu_icon'           => 'dashicons-chart-area',
			'supports'            => $supports,
			'hierarchical'        => false,
			'show_ui'             => true,
			'exclude_from_search' => true,
			'show_in_menu'        => true,
			'show_in_rest'        => true,
		];

		register_post_type( 'analytics_triggers', $args );

	}

	/**
	 * Register custom taxonomy
	 *
	 * @return void
	 */
	// public function analytics_type_taxonomy() {
	//
	// 	$args = array(
	// 		"label" => "Calculator Types",
	// 		"public" => true,
	// 		"rewrite" => array( 'slug' => 'analytics_type', 'with_front' => true, ),
	// 		"hierarchical" => true,
	// 		"show_in_menu" => false,
	// 		"show_in_quick_edit" => true,
	// 	);
	//
	// 	register_taxonomy( "analytics_type", array( "analytics" ), $args );
	// }

	/**
	 * Register acf fields
	 *
	 * @return void
	 */
	public function analytics_acf() {

		if( function_exists('acf_add_local_field_group') ):

			acf_add_local_field_group(array(
				'key' => 'group_5c7b0736ab9b6',
				'title' => 'Analytics - Goals',
				'fields' => array(
					array(
						'key' => 'field_5c7b075871eb9',
						'label' => 'Page',
						'name' => 'page',
						'type' => 'post_object',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => array(
							0 => 'page',
							1 => 'post',
						),
						'taxonomy' => '',
						'allow_null' => 0,
						'multiple' => 0,
						'return_format' => 'id',
						'ui' => 1,
					),
					array(
						'key' => 'field_5c7b0a0571eba',
						'label' => 'Trigger',
						'name' => 'trigger',
						'type' => 'select',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'choices' => array(
							'Not Selected' => 'Not Selected',
							'Start Loan Application' => 'Start Loan Application',
							'Start Account Application' => 'Start Account Application',
							'Finish Loan Application' => 'Finish Loan Application',
							'Finish Account Application' => 'Finish Account Application',
						),
						'default_value' => array(
						),
						'allow_null' => 0,
						'multiple' => 0,
						'ui' => 0,
						'return_format' => 'value',
						'ajax' => 0,
						'placeholder' => '',
					),
					array(
						'key' => 'field_5c7b0c1c89egf',
						'label' => 'Element',
						'name' => 'element',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c7b0b4b71ebb',
						'label' => 'Selector',
						'name' => 'selector',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c7b0c1c71ebc',
						'label' => 'Label',
						'name' => 'label',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'analytics_triggers',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

		endif;

	}

}
