<?php
namespace Analytics;

/**
 * REST_API Handler
 */
class REST_API {

	public function __construct() {
		add_action( 'rest_api_init', function() {

			// Create Goal
			register_rest_route( 'analytics/v1', '/create-goal', [
				'methods'  => 'GET',
				'callback' => [ $this, 'create_goal' ],
			]);

			// Delete Goal
			register_rest_route( 'analytics/v1', '/delete-goal', [
				'methods'  => 'GET',
				'callback' => [ $this, 'delete_goal' ],
			]);

		} );
	}

	public function create_goal( $data ) {

		// Check that user is authenticated
		if ( !check_ajax_referer('wp_rest', '_wpnonce', false) ) {

			// Not authenticated
			return 'You are not allowed to do that.';

		} else {

			// Authenticated

			// Get data from request
			$trigger  = $data['trigger'];
			$element  = $data['element'];
			$selector = $data['selector'];
			$label    = $data['label'];
			$pageId   = $data['pageId'];
			$pageName = $data['pageName'];

			// Setup new post
			$new_post = array(
				'post_type' => 'analytics_triggers',
				'post_title' => $pageName . ' - ' . $label,
				'post_author' => 1,
				'post_status' => 'publish',
			);

			// Create post, returns the post's id
			$new_post_id = wp_insert_post( $new_post, true );

			// Update new post's meta fields
			update_post_meta(
				$post_id = $new_post_id,
				$key = 'page',
				$value = $pageId
			);

			update_post_meta(
				$post_id = $new_post_id,
				$key = 'trigger',
				$value = $trigger
			);

			update_post_meta(
				$post_id = $new_post_id,
				$key = 'element',
				$value = $element
			);

			update_post_meta(
				$post_id = $new_post_id,
				$key = 'selector',
				$value = $selector
			);

			update_post_meta(
				$post_id = $new_post_id,
				$key = 'label',
				$value = $label
			);

			return $new_post_id;

		}

	}

	public function delete_goal( $data ) {

		// Check that user is authenticated
		if ( !check_ajax_referer('wp_rest', '_wpnonce', false) ) {

			// Not authenticated
			return 'You are not allowed to do that.';

		} else {

			// Authenticated

			// Get goal id from request
			$goalId = $data['goalId'];

			// Delete the goal
			wp_delete_post( $goalId, false ); // false = skip trash

			return 'Goal deleted.';

		}

	}

}
