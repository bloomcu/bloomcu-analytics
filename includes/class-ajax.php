<?php
namespace Analytics;

/**
 * REST_API Handler
 */
class Ajax {

	public function __construct() {
		add_action( 'wp_ajax_analytics_save_analytics', [ $this, 'save_analytics' ] );
	}

	public function save_analytics() {

		$post_id   = sanitize_text_field( $_POST['id'] );
		$analytics_type = sanitize_text_field( $_POST['type'] );
		$analytics_json = $_POST['schema'];

		if ( empty( $post_id ) || empty( $analytics_json ) ) {
			return;
		}

		$this->save_postdata( $post_id, $analytics_json, $analytics_type );

		wp_die();

	}

	/**
	 * Save Quiz Data
	 */
	public function save_postdata( $post_id, $schema, $type ) {

		update_post_meta(
			$post_id,
			'_bloom_analytics_schema',
			wp_slash( $schema )
		);

		update_post_meta(
			$post_id,
			'_bloom_analytics_type',
			$type
		);

	}

}
