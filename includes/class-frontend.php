<?php
namespace Analytics;

/**
 * Frontend Pages Handler
 */
class Frontend {

	public function __construct() {

		// Is goal mode active
		// $goal_mode_param  = isset( $_GET['goal-mode'] );
		$goal_mode_cookie = isset( $_COOKIE['bloom_goalmode_on'] );

		/**
		* Admin - Init Goal Mode
		*/
		if ( is_user_logged_in() && $goal_mode_cookie ) {

			add_action( 'wp_footer', [ $this, 'init_goal_mode' ], 5 );

		/**
		* Admin - Init trigger
		*/
		} elseif ( is_user_logged_in() ) {

			add_action( 'wp_footer', [ $this, 'init_goal_mode_trigger' ], 5 );

		/**
		* Non-Admin - Init Goal Listeners
		*/
		} else {

			add_action( 'wp_footer', [ $this, 'init_goal_listeners' ], 5 );

		}

	}

	/**
	 * Init Goal Mode
	 *
	 */
	public function init_goal_mode() {
		wp_enqueue_style( 'analyticsplugin-frontend' );
		wp_enqueue_script( 'analyticsplugin-frontend' );

		$args = array(
			'post_type' => 'analytics_triggers',
			'numberposts'=> -1,
			'post_status' => 'publish',
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'page',
					'value' => get_the_ID(),
					'compare' => '='
				)
			)
		);

		// Get posts
		$posts = get_posts( $args );

		ob_start();

		?>

		<style media="screen">
			/* Make space for sidebar */
			body { margin-right: 320px !important; }

			/* Hide WP admin bar */
			#wpadminbar {
				display: none;
			}
		</style>

		<div class="bloom-sidebar">

		    <div id="bloom-sidebar-main" class="bloom-sidebar-container">

				<div class="bloom-sidebar-header">

					<div class="bloom-sidebar-header-left">
						<svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
						    <defs>
						        <linearGradient x1="19.0631093%" y1="67.2527235%" x2="80.9368907%" y2="67.2527235%" id="linearGradient-1">
						            <stop stop-color="#E8006E" offset="0%"></stop>
						            <stop stop-color="#F41E1E" offset="100%"></stop>
						        </linearGradient>
						    </defs>
						    <g id="Artboard" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						        <path d="M24.4394649,5.72793645 L24.4394649,14.130233 C24.4394649,19.4550447 20.1228521,23.7716575 14.7980403,23.7716575 L14.7980403,15.369361 C14.7980403,10.0445493 19.1146531,5.72793645 24.4394649,5.72793645 Z M-1.63424829e-13,1.13686838e-13 C7.01516059,1.13686838e-13 12.7020719,5.67623125 12.7020719,12.6782173 L12.7020719,23.7716575 C5.68691129,23.7716575 -7.28012773e-14,18.0954263 -7.28012773e-14,11.0934402 L-1.63424829e-13,1.13686838e-13 Z" id="Combined-Shape" fill="url(#linearGradient-1)" fill-rule="nonzero"></path>
						    </g>
						</svg>
						GoalMode
					</div>

					<button class="bloom-sidebar-header-right bloom-goalmode-stop is-bloom-app-element">
						<svg width="21px" height="23px" viewBox="0 0 21 23" version="1.1" xmlns="http://www.w3.org/2000/svg">
						    <g id="Artboard-Copy" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						        <g id="noun_Power_404078" fill="#FFFFFF" fill-rule="nonzero">
						            <path d="M16.4652058,4.13049559 L15.7810481,3.6251841 L14.7619086,4.99349948 L15.4460663,5.49881097 C18.4392998,7.7059168 19.6771859,11.5853873 18.5155046,15.1182698 C17.3538232,18.6511524 14.0551482,21.0389122 10.3361749,21.0389122 C6.61720155,21.0389122 3.31852658,18.6511524 2.15684519,15.1182698 C0.995163813,11.5853873 2.23304998,7.7059168 5.22628342,5.49881097 L5.91044111,4.99349948 L4.89981814,3.62234527 L4.21566045,4.12765676 C0.631703334,6.77174123 -0.850099701,11.4177482 0.541147329,15.6486302 C1.93239436,19.8795123 5.88236533,22.7393811 10.3361199,22.7404133 C14.7898746,22.7414455 18.7411707,19.8834077 20.1343786,15.6531709 C21.5275864,11.4229342 20.047937,6.77624093 16.4652058,4.13049559 L16.4652058,4.13049559 Z" id="Path"></path>
						            <rect id="Rectangle" x="9.49872045" y="0.0397436002" width="1.70329715" height="13.5951501"></rect>
						        </g>
						    </g>
						</svg>
					</button>

				</div>

				<div class="bloom-sidebar-content bloom-goal-list">

					<div class="bloom-sidebar-content-title">Goals</div>

					<?php

					if ( $posts ) {
						foreach ( $posts as $post ) {

							$id = $post->ID;
							$label = get_field( 'label', $post->ID );
							$trigger = get_field( 'trigger', $post->ID );
							$selector = get_field( 'selector', $post->ID );
							?>

							<div class="bloom-single-goal bloom-card">

								<div class="bloom-card-left">
									<svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg">
									    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
									        <rect x="0" y="0" width="16.7000017" height="16.7000017"></rect>
									        <path d="M3.07754319,7.35412918 C3.39062659,7.56729234 3.47056277,8.00028002 3.25739961,8.32002477 C3.0375751,8.63310817 2.60458742,8.71304435 2.29150402,8.49321984 C1.07913852,7.66721258 0.353051491,6.28831336 0.353051491,4.81615525 C0.353051491,3.67706459 0.792700518,2.53131258 1.66533722,1.66533722 C2.53131258,0.792700518 3.67706459,0.353051491 4.81615525,0.353051491 C5.96190726,0.353051491 7.10099792,0.792700518 7.97363463,1.66533722 C8.16681374,1.85185499 8.34000881,2.06501816 8.49321984,2.29150402 C8.71304435,2.60458742 8.63310817,3.0375751 8.32002477,3.25739961 C8.00028002,3.47056277 7.56729234,3.39062659 7.35412918,3.07754319 C7.2475476,2.92433216 7.12098197,2.77778249 6.98775499,2.64455551 C6.38823359,2.04503411 5.60219442,1.74527341 4.81615525,1.74527341 C4.03011608,1.74527341 3.24407691,2.04503411 2.64455551,2.64455551 C2.04503411,3.24407691 1.74527341,4.03011608 1.74527341,4.81615525 C1.74527341,5.82201893 2.25153593,6.78125317 3.07754319,7.35412918 Z M4.88943009,4.00347069 L15.8739944,7.78045551 C16.2337073,7.90702114 16.4268864,8.30004072 16.3069821,8.66641491 C16.2337073,8.86625538 16.0871576,9.01280505 15.9006398,9.08607989 L11.0511778,11.0511778 L9.08607989,15.9073012 C8.93953022,16.267014 8.53318793,16.4335477 8.18013644,16.2936594 C7.98695732,16.2137232 7.847069,16.0605122 7.78045551,15.8739944 L4.00347069,4.88943009 C3.88356641,4.52971725 4.07674552,4.13003631 4.43645836,4.00347069 C4.58966939,3.95684124 4.74954176,3.95684124 4.88943009,4.00347069 L4.88943009,4.00347069 Z" id="Shape" fill="#E8006E" fill-rule="nonzero"></path>
									    </g>
									</svg>
								</div>

								<div class="bloom-card-body">
									<div class="bloom-card-body-label"><?php echo strtolower($label); ?></div>
									<div class="bloom-card-body-trigger"><?php echo $trigger; ?></div>
								</div>

								<div class="bloom-card-right">
									<button class="bloom-single-goal-delete is-bloom-app-element" data-id="<?php echo $id; ?>">
										<svg width="12px" height="12px" viewBox="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg">
										    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
										        <path d="M1.08910393,1.2962281 L10.4978082,10.7049324" stroke="#000000" stroke-width="1.5"></path>
										        <path d="M1.08910393,1.2962281 L10.4978082,10.7049324" stroke="#000000" stroke-width="1.5" transform="translate(5.811612, 6.018737) scale(-1, 1) translate(-5.811612, -6.018737) "></path>
										    </g>
										</svg>
									</button>
								</div>

							</div>

						<?php
						}

						wp_reset_postdata();

					} else {
						?>

						<div class="bloom-goal-list-message" style="color: #fff; text-align: center;">Click a highlighted element to create a goal.</div>

						<?php
					}

					?>
				</div>

				<div class="bloom-sidebar-footer">

					<div class="bloom-switch-container">
						<div class="bloom-switch-container-switch">
							<input type="checkbox" />
							<span class="is-bloom-app-element slider round"></span>
						</div>
						<span class="bloom-switch-container-switch-label">On</span>
					</div>

				</div>

		    </div>

			<div id="bloom-sidebar-new" class="bloom-sidebar-container" style="display: none;">

				<div class="bloom-sidebar-header">

					<div class="bloom-sidebar-header-left">
						New Goal
					</div>

					<button class="bloom-sidebar-header-right bloom-close-sidebar-new is-bloom-app-element">
						<svg width="20px" height="19px" viewBox="0 0 20 19" version="1.1" xmlns="http://www.w3.org/2000/svg">
						    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="square">
						        <path d="M2.15147669,1.50358777 L18.1462739,17.498385" id="Line-5" stroke="#FFFFFF" stroke-width="1.8"></path>
						        <path d="M2.15147669,1.50358777 L18.1462739,17.498385" id="Line-5" stroke="#FFFFFF" stroke-width="1.8" transform="translate(10.179741, 9.531852) scale(-1, 1) translate(-10.179741, -9.531852) "></path>
						    </g>
						</svg>
					</button>

				</div>

				<div class="bloom-sidebar-content">

					<div class="bloom-sidebar-content-title">Category</div>

					<div class="bloom-sidebar-content-triggers">

						<button class="bloom-card bloom-single-trigger is-bloom-app-element" data-trigger="Start Account Application">
							<div class="bloom-card-left">
								<svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="16.7000017" height="16.7000017"></rect>
										<path d="M3.07754319,7.35412918 C3.39062659,7.56729234 3.47056277,8.00028002 3.25739961,8.32002477 C3.0375751,8.63310817 2.60458742,8.71304435 2.29150402,8.49321984 C1.07913852,7.66721258 0.353051491,6.28831336 0.353051491,4.81615525 C0.353051491,3.67706459 0.792700518,2.53131258 1.66533722,1.66533722 C2.53131258,0.792700518 3.67706459,0.353051491 4.81615525,0.353051491 C5.96190726,0.353051491 7.10099792,0.792700518 7.97363463,1.66533722 C8.16681374,1.85185499 8.34000881,2.06501816 8.49321984,2.29150402 C8.71304435,2.60458742 8.63310817,3.0375751 8.32002477,3.25739961 C8.00028002,3.47056277 7.56729234,3.39062659 7.35412918,3.07754319 C7.2475476,2.92433216 7.12098197,2.77778249 6.98775499,2.64455551 C6.38823359,2.04503411 5.60219442,1.74527341 4.81615525,1.74527341 C4.03011608,1.74527341 3.24407691,2.04503411 2.64455551,2.64455551 C2.04503411,3.24407691 1.74527341,4.03011608 1.74527341,4.81615525 C1.74527341,5.82201893 2.25153593,6.78125317 3.07754319,7.35412918 Z M4.88943009,4.00347069 L15.8739944,7.78045551 C16.2337073,7.90702114 16.4268864,8.30004072 16.3069821,8.66641491 C16.2337073,8.86625538 16.0871576,9.01280505 15.9006398,9.08607989 L11.0511778,11.0511778 L9.08607989,15.9073012 C8.93953022,16.267014 8.53318793,16.4335477 8.18013644,16.2936594 C7.98695732,16.2137232 7.847069,16.0605122 7.78045551,15.8739944 L4.00347069,4.88943009 C3.88356641,4.52971725 4.07674552,4.13003631 4.43645836,4.00347069 C4.58966939,3.95684124 4.74954176,3.95684124 4.88943009,4.00347069 L4.88943009,4.00347069 Z" id="Shape" fill="#E8006E" fill-rule="nonzero"></path>
									</g>
								</svg>
							</div>
							<div class="bloom-card-body">
								<div class="bloom-card-body-label">Account</div>
							</div>
						</button>

						<button class="bloom-card bloom-single-trigger selected is-bloom-app-element" data-trigger="Start Loan Application">
							<div class="bloom-card-left">
								<svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<rect x="0" y="0" width="16.7000017" height="16.7000017"></rect>
										<path d="M3.07754319,7.35412918 C3.39062659,7.56729234 3.47056277,8.00028002 3.25739961,8.32002477 C3.0375751,8.63310817 2.60458742,8.71304435 2.29150402,8.49321984 C1.07913852,7.66721258 0.353051491,6.28831336 0.353051491,4.81615525 C0.353051491,3.67706459 0.792700518,2.53131258 1.66533722,1.66533722 C2.53131258,0.792700518 3.67706459,0.353051491 4.81615525,0.353051491 C5.96190726,0.353051491 7.10099792,0.792700518 7.97363463,1.66533722 C8.16681374,1.85185499 8.34000881,2.06501816 8.49321984,2.29150402 C8.71304435,2.60458742 8.63310817,3.0375751 8.32002477,3.25739961 C8.00028002,3.47056277 7.56729234,3.39062659 7.35412918,3.07754319 C7.2475476,2.92433216 7.12098197,2.77778249 6.98775499,2.64455551 C6.38823359,2.04503411 5.60219442,1.74527341 4.81615525,1.74527341 C4.03011608,1.74527341 3.24407691,2.04503411 2.64455551,2.64455551 C2.04503411,3.24407691 1.74527341,4.03011608 1.74527341,4.81615525 C1.74527341,5.82201893 2.25153593,6.78125317 3.07754319,7.35412918 Z M4.88943009,4.00347069 L15.8739944,7.78045551 C16.2337073,7.90702114 16.4268864,8.30004072 16.3069821,8.66641491 C16.2337073,8.86625538 16.0871576,9.01280505 15.9006398,9.08607989 L11.0511778,11.0511778 L9.08607989,15.9073012 C8.93953022,16.267014 8.53318793,16.4335477 8.18013644,16.2936594 C7.98695732,16.2137232 7.847069,16.0605122 7.78045551,15.8739944 L4.00347069,4.88943009 C3.88356641,4.52971725 4.07674552,4.13003631 4.43645836,4.00347069 C4.58966939,3.95684124 4.74954176,3.95684124 4.88943009,4.00347069 L4.88943009,4.00347069 Z" id="Shape" fill="#E8006E" fill-rule="nonzero"></path>
									</g>
								</svg>
							</div>
							<div class="bloom-card-body">
								<div class="bloom-card-body-label">Loan</div>
							</div>
						</button>

					</div>

					<div class="bloom-sidebar-content-title">Element</div>

					<div class="bloom-sidebar-content-details">

						<table>
							<tr hidden>
								<td>Page</td>
								<td id="page-name"><?php echo get_the_title(); ?></td>
							</tr>
							<tr>
								<td>Trigger</td>
								<td id="trigger"></td>
							</tr>
							<tr>
								<td>Element</td>
								<td id="element"></td>
							</tr>
							<tr hidden>
								<td>Selector</td>
								<td id="selector"></td>
							</tr>
							<tr>
								<td>Label</td>
								<td id="label"></td>
							</tr>
						</table>

						<span id="page-id" style="display: none"><?php echo get_the_ID(); ?></span>

					</div>

				</div>

				<div class="bloom-sidebar-footer">

					<button class="bloom-sidebar-footer-create is-bloom-app-element">Create Goal</button>

				</div>

			</div>

		</div>

		<?php
		echo ob_get_clean();
	}

	/**
	 * Init Trigger
	 *
	 */
	public function init_goal_mode_trigger() {
		wp_enqueue_style( 'analyticsplugin-frontend' );
		wp_enqueue_script( 'analyticsplugin-frontend' );

		ob_start();

		?>

		<div class="bloom-goalmode-trigger-dot">
			<svg width="25px" height="24px" viewBox="0 0 25 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
			    <defs>
			        <linearGradient x1="19.0631093%" y1="67.2527235%" x2="80.9368907%" y2="67.2527235%" id="linearGradient-1">
			            <stop stop-color="#E8006E" offset="0%"></stop>
			            <stop stop-color="#F41E1E" offset="100%"></stop>
			        </linearGradient>
			    </defs>
			    <g id="Artboard" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
			        <path d="M24.4394649,5.72793645 L24.4394649,14.130233 C24.4394649,19.4550447 20.1228521,23.7716575 14.7980403,23.7716575 L14.7980403,15.369361 C14.7980403,10.0445493 19.1146531,5.72793645 24.4394649,5.72793645 Z M-1.63424829e-13,1.13686838e-13 C7.01516059,1.13686838e-13 12.7020719,5.67623125 12.7020719,12.6782173 L12.7020719,23.7716575 C5.68691129,23.7716575 -7.28012773e-14,18.0954263 -7.28012773e-14,11.0934402 L-1.63424829e-13,1.13686838e-13 Z" id="Combined-Shape" fill="url(#linearGradient-1)" fill-rule="nonzero"></path>
			    </g>
			</svg>
		</div>

		<?php
		echo ob_get_clean();

	}

	/**
	 * Init Goal Listeners
	 *
	 */
	public function init_goal_listeners() {

		$args = array(
			'post_type' => 'analytics_triggers',
			'numberposts'=> -1,
			'post_status' => 'publish',
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'page',
					'value' => get_the_ID(),
					'compare' => '='
				)
			)
		);

		// Get posts
		$posts = get_posts( $args );

		ob_start();

		if ( $posts ) {

			echo '<script>';

				// Init already present dataLayer array from GTM
				// Else, init as new array
				echo 'window.dataLayer = window.dataLayer || [];';

				// When the entire page loads
				echo 'window.onload = function() {';

				foreach ( $posts as $post ) {

					// $id = $post->ID;
					$trigger  = get_field( 'trigger', $post->ID );
					$element  = get_field( 'element', $post->ID );
					$selector = get_field( 'selector', $post->ID );
					$label    = get_field( 'label', $post->ID );

					if ($element == 'form') {

						echo '
							$(document).on( "gform_confirmation_loaded", function() {

								window.dataLayer.push({
									"event": "'.$trigger.'"
								});

								console.log("Form submitted, triggered '.$trigger.'");
							});
						';

					} else {

						echo '
							$( "'.$selector.'" ).one( "click", function() {

								window.dataLayer.push({
									"event": "'.$trigger.'"
								});

								console.log("Button clicked, triggered '.$trigger.'");
							});
						';

					}

				}

				echo '};';
			echo '</script>';

		}

		echo ob_get_clean();

	}

}
