<?php
namespace Analytics;

/**
 * Admin Pages Handler
 */
class Admin {

	public function __construct() {
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_filter( 'manage_analytics_posts_columns', [ $this, 'set_post_columns' ] );
		// add_action( 'manage_analytics_posts_custom_column', [ $this, 'set_columns_content' ], 10, 2 );
	}

	/**
	 * Inject our metabox into the analytics post type
	 *
	 * @return void
	 */
	public function admin_menu() {


	}

	/**
	 * Add custom columns
	 *
	 * @return Array
	 */
	public function set_post_columns( $columns ) {
		$columns = [
			'cb'        => $columns['cb'],
			'title'     => 'Title',
			// 'type'      => 'Type',
			// 'shortcode' => 'Shortcode',
			'date'      => $columns['date'],
		];

		return $columns;
	}

	/**
	 * Set columns content
	 */
	public function set_columns_content( $column, $post_id ) {

		// $analytics_type = get_the_terms( $post_id, 'analytics_type');

		// switch ( $column ) {
		//
		// 	case 'type':
		// 		echo esc_html( $analytics_type[0]->name );
		// 		break;
		//
		// 	case 'shortcode':
		// 		echo "[analytics id=\"$post_id\"]";
		// 		break;
		// }
	}

}
