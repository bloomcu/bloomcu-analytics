# BloomCU Analytics

Analytics events editor. A WordPress Plugin by BloomCU.

## Changelog

**1.0.1**
*Release Date – 10 October 2019*
* Add Gravity form Ajax submission tracking
* Save element type to post type (e.g., "a", "button", "form")
* Minor style update
* Namespace Webpack output

**1.0.0**
*Release Date – 13 May 2019*
* Initial Release
